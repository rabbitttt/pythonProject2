"""
@author : rabbit
@Date : 2022/3/22  19:20
"""


def smallestSubarray(list_num):
    if len(list_num) == 0:
        return ''
    else:
        result = list_num[0]
    for i in range(0, len(list_num)):
        num = 0
        for j in range(len(list_num) - 1, i - 1, -1):
            num += list_num[j]
            if num > result:
                result = num
            elif num < 0:
                num = 0
    return result
