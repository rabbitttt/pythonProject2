"""
@author : rabbit
@Date : 2022/4/19  10:45
"""
from django.urls import re_path, path

from .search_indexes import SKUSearchView
from .views import ListView, HotView, DetailView, DetailVisitView, HistoryView

urlpatterns = [
    re_path(r'^list/(?P<category_id>\d+)/(?P<page_num>\d+)/', ListView.as_view()),
    re_path(r'^hot/(?P<category_id>\d+)/', HotView.as_view()),
    re_path(r'^detail/(?P<sku_id>\d+)/', DetailView.as_view()),
    re_path(r'^detail/visit/(?P<category_id>\d+)/', DetailVisitView.as_view()),
    re_path(r'^browse_histories/', HistoryView.as_view()),
    re_path(r'search/', SKUSearchView()),
]
