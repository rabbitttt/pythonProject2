"""
@author : rabbit
@Date : 2022/5/5  16:21
"""
from django.shortcuts import render
from haystack import indexes
from haystack.views import SearchView

from .models import SKU
from mark.utils.goods import get_categories


class SKUIndex(indexes.SearchIndex, indexes.Indexable):
    """属性text不可修改"""
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        """用于搜索的表"""
        return SKU

    def index_queryset(self, using=None):
        """指定哪些行的数据在搜索范围内"""
        return self.get_model().objects.filter(is_launched=True)


class SKUSearchView(SearchView):

    def create_response(self):
        # 获取搜索结果
        context = self.get_context()
        categories = get_categories()
        context['categories'] = categories

        #
        sku_list = []
        for item in context['page'].object_list:
            sku_list.append({
                'id': item.object.id,
                'name': item.object.name,
                'price': item.object.price,
                'default_image_url': 'item.object.default_image',
                'searchkey': context.get('query'),
                'page_size': context['page'].paginator.num_pages,
                'count': context['page'].paginator.count
            })
        return render(self.request, self.template, context)
