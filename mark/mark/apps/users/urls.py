"""
@author : rabbit
@Date : 2022/4/19  10:35
"""
from django.urls import re_path,path

from . import views

urlpatterns = [
    # 用户注册 reverse(user:register) == '/register/'
    re_path(r'^register/', views.RegisterView.as_view(), name='register'),
    # 判断用户名是否重复注册
    re_path(r'usernames/(?P<username>[a-zA-Z0-9_-]{5,20})/counts/', views.UsernameCountView.as_view()),
    # 判断手机号是否重复
    re_path(r'mobiles/(?P<mobile>^1[345789]\d{9})/counts/', views.UsernameCountView.as_view()),
    # 判断邮箱是否重复
    re_path(r'emails/(?P<email>^\w+@[\da-z\.-]+\.com$)/counts/', views.EmailCountView.as_view()),
    # 用户登录
    re_path(r'^login/$', views.LoginView.as_view(), name='login'),
    # 用户退出登录
    re_path(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    # 用户中心
    re_path(r'^info/$', views.UserInfoView.as_view(), name='info'),
    re_path(r'login.html?', views.UserInfoView.as_view(), name='info'),
    re_path('^sms_codes/', views.SmsCodeView.as_view())
]
