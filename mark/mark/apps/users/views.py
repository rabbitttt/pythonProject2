import re
from random import randint

from django import http
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.db import DatabaseError
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django_redis import get_redis_connection
from users.models import User

# Create your views here.
from mark.settings import dev


class SmsCodeView(View):

    def get(self, request):
        path = request.path.split('/')
        email = path[2]
        if not email:
            return http.HttpResponseForbidden('缺少必传参数')
        redis_cli = get_redis_connection('code')
        sms_code = '%06d' % randint(0, 999999)
        redis_cli.setex(email, 3600 * 24 * 15, sms_code)
        send_status = send_mail(
            subject='注册邮箱验证码',
            message="您的邮箱注册验证码为：{0}, 该验证码有效时间为5分钟，请及时进行验证。".format(sms_code),
            recipient_list=[email],
            from_email=dev.EMAIL_HOST_USER
        )
        if send_status:
            return http.HttpResponse('邮件已发送')


class UserInfoView(LoginRequiredMixin, View):
    """用户中心"""

    def get(self, request):
        """提供界面"""
        return render(request, 'user_center_info.html')


class LogoutView(View):
    """用户退出登录"""

    def get(self, request):
        """实现用户退出登录的逻辑"""
        # 清除状态保持信息
        logout(request)

        # 退出后定向到首页
        response = redirect(reverse('contents:index'))
        # 删除cookie中的用户名
        response.delete_cookie('username')

        # 响应结果
        return response


class LoginView(View):
    """用户登录"""

    def get(self, request):
        """提供用户登录界面"""
        return render(request, 'login.html')

    @csrf_exempt
    def post(self, request):
        """实现用户登录逻辑"""
        # 接受参数
        username = request.POST.get('username')
        password = request.POST.get('pwd')
        remember = request.POST.get('remember')

        # 校验参数
        if not all([username, password]):
            return http.HttpResponseForbidden('缺少必传参数')
        # 判断用户名长度
        if not re.match(r'^[a-zA-Z0-9_-]{5,20}$', username):
            return http.HttpResponseForbidden('请输入5-20个字符的用户名')
        # 判断密码长度
        if not re.match(r'^[0-9A-Za-z]{8,20}$', password):
            return http.HttpResponseForbidden('请输入8-20个字符的密码')

        # 认证用户
        user = authenticate(username=username, password=password)
        if user is None:
            return render(request, 'login.html', {'account_errmsg': '账号或密码错误'})

        # 保持状态
        login(request, user)
        # 设置状态保持周期
        if remember != 'on':
            # 没有记住用户，浏览器关闭就会结束
            request.session.set_expiry(0)
        else:
            request.session.set_expiry(None)

        # 响应结果
        # 取出next
        next = request.GET.get('next')
        if next:
            response = redirect(next)
        else:
            response = redirect(reverse('contents:index'))
        # 实现右上角显示用户名信息
        response.set_cookie('username', user.username, max_age=3600 * 24 * 15)
        return response


class EmailCountView(View):
    """判断邮箱是否重复"""

    def get(self, request, email):
        # 实现主体业务逻辑：使用username查询对应的记录的条数。filter返回满足条件的结果集
        count = User.objects.filter(email=email).count()
        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': 'OK', 'count': count})


class MobileCountView(View):
    """判断手机号是否重复"""

    def get(self, request, mobile):
        # 实现主体业务逻辑：使用username查询对应的记录的条数。filter返回满足条件的结果集
        count = User.objects.filter(mobile=mobile).count()
        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': 'OK', 'count': count})


class UsernameCountView(View):
    """判断用户名是否重复注册"""

    def get(self, request, username):
        # 实现主体业务逻辑：使用username查询对应的记录的条数。filter返回满足条件的结果集
        count = User.objects.filter(username=username).count()
        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': 'OK', 'count': count})


class RegisterView(View):
    # 用户注册
    def get(self, request):
        """提供用户注册页面"""
        return render(request, 'register.html')

    def post(self, request):
        """实现用户注册业务逻辑"""
        # 接受参数:表单参数
        username = request.POST.get('user_name')
        password = request.POST.get('pwd')
        password2 = request.POST.get('cpwd')
        mobile = request.POST.get('phone')
        email = request.POST.get('email')
        profession = request.POST.get('profession')
        smscode = request.POST.get('msg_code')
        allow = request.POST.get('allow')

        # 校验参数:前后端分离校验，避免恶意用户，保证后端安全，校验逻辑相同
        # 判断参数是否齐全
        # if not all([username, password, password2, mobile, email, profession, smscode,
        if not all([username, password, password2, mobile, email, profession,
                    allow]):  # all([])会去校验列表中的元素是否为空，只要有一个为空返回false
            return http.HttpResponseForbidden('缺少必传参数')
        # 判断用户名长度
        if not re.match(r'^[a-zA-Z0-9_-]{5,20}$', username):
            return http.HttpResponseForbidden('请输入5-20个字符的用户名')
        # 判断密码长度
        if not re.match(r'^[0-9A-Za-z]{8,20}$', password):
            return http.HttpResponseForbidden('请输入8-20个字符的密码')
        # 判断两次密码是否一致
        if password2 != password:
            return http.HttpResponseForbidden('两次输入密码不一致')
        # 判断手机号是否合法
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return http.HttpResponseForbidden('请输入正确手机号')
        # 判断邮箱是否合法
        if not re.match(r'^\w+@[\da-z\.-]+\.com$', email):
            return http.HttpResponseForbidden('请输入正确邮箱')
        # 判断验证码是否正确
        redis_cli = get_redis_connection('code')
        ver = redis_cli.get(email).decode('utf-8')
        # if ver != smscode:
        #     return http.HttpResponseForbidden('请输入正确验证码')
        # 判断用户是否勾选了协议
        if allow != 'on':
            return http.HttpResponseForbidden('请勾选用户协议')

        # 保存注册数据：注册业务的核心
        try:
            user = User.objects.create_user(username=username, email=email, password=password, mobile=mobile,
                                            profession=profession)
        except DatabaseError:
            return render(request, 'register.html', {'register_errmsg': '注册失败'})

        # 实现状态保持
        login(request, user)
        # 响应结果:重定向到首页
        response = redirect(reverse('contents:index'))

        response.set_cookie('username', user.username, max_age=3600 * 24 * 15)

        return response
