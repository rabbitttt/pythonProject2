"""
@author : rabbit
@Date : 2022/4/19  10:35
"""

# 自定义用户后端：实现多账号登录
import re

from django.contrib.auth.backends import ModelBackend
from users.models import User


def get_user_by_account(account):
    """通过账号获取用户"""
    try:
        if re.match(r'^1[3-9]\d{9}$', account):
            # username是手机号
            user = User.objects.get(mobile=account)
        else:
            # username是用户名
            user = User.objects.get(username=account)
    except User.DoesNotExist:
        return None
    else:
        return user


class UsernameMobileBackend(ModelBackend):
    """自定义用户后端认证"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        """重写用户认证的方法"""
        # 查询用户
        user = get_user_by_account(username)

        # 如果可以查到用户，校验密码是否正确
        if user and user.check_password(password):
            # 返回user
            return user
        else:
            return None
