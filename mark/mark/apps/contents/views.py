from django.shortcuts import render
from django.views import View

from mark.utils.goods import get_categories
from contents.models import ContentCategory


# Create your views here.


class IndexView(View):
    """首页"""

    def get(self, request):
        """提供首页页面"""
        # 1. 商品分类数据
        categories = get_categories()
        # 2. 广告数据
        contents = {}
        content_categories = ContentCategory.objects.all()
        for cat in content_categories:
            contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')
        # 实现首页静态化，将数据传给模板
        context={
            'categories':categories,
            'contents':contents
        }
        return render(request, 'index.html',context)
        # return render(request, 'index.html')
