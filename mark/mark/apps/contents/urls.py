"""
@author : rabbit
@Date : 2022/4/19  10:33
"""
from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^$', views.IndexView.as_view(), name='index')
]