"""
@author : rabbit
@Date : 2022/4/26  20:36
"""

from goods.models import GoodsChannel, GoodsCategory


def get_categories():
    categories = {}
    channels = GoodsChannel.objects.order_by('group_id', 'sequence')
    for channel in channels:
        group_id = channel.group_id
        if group_id not in categories:
            categories[group_id] = {
                'channels': [],
                'sub_cats': []
            }
        cat1 = channel.category
        categories[group_id]['channels'].append({
            'id': cat1.id,
            'name': cat1.name,
            'url': channel.url
        })
        cat2s = GoodsCategory.objects.filter(parent=cat1)
        for cat2 in cat2s:
            cat2.sub_cats = []
            cat3s = GoodsCategory.objects.filter(parent=cat2)
            for cat3 in cat3s:
                cat2.sub_cats.append(cat3)
            categories[group_id]['sub_cats'].append(cat2)
    return categories


def get_breadcrumb(category):
    dict = {
        'cat1': '',
        'cat2': '',
        'cat3': ''
    }
    if category.parent is None:
        dict['cat1'] = category.name
    elif category.parent.parent is None:
        dict['cat2'] = category.name
        dict['cat1'] = category.parent.name
    else:
        dict['cat3'] = category.name
        dict['cat2'] = category.parent.name
        dict['cat1'] = category.parent.parent.name
    return dict


def get_goods_specs(sku):
    sku_specs = sku.specs.order_by('spec_id')
    sku_key = []
    for spec in sku_specs:
        sku_key.append(spec.option.id)

    skus = sku.spu.sku_set.all()
    spec_sku_map = {}
    for s in skus:
        s_specs = s.specs.order_by('spec_id')
        key = []
        for spec in s_specs:
            key.append(spec.option.id)
            spec_sku_map[tuple(key)] = s.id
    goods_specs = sku.spu.specs.order_by('id')
    if len(sku_key) < len(goods_specs):
        return
    for index, spec in enumerate(goods_specs):
        key = sku_key[:]
        spec_options = spec.option.all()
        for option in spec_options:
            key[index] = option.id
            option.sku_id = spec_sku_map.get(tuple(key))
        spec.spec_options = spec_options
    return goods_specs
