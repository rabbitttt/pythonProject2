"""
@author : Administrator
@Date : 2022/3/28  16:02
"""
import sys
import softwareTest
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QInputDialog, QTextBrowser, QLineEdit

'''
按钮功能：下一题，查看正确答案
显示上只显示一题，一个输入框输入正确答案       
'''
list_num = softwareTest.program()


class calculate(QWidget):
    def __init__(self):
        super().__init__()
        self.InitUI()
        self.i = 0

    def InitUI(self):
        self.setGeometry(400, 400, 400, 400)
        self.setWindowTitle('四则运算生成器')
        self.test1 = QLabel(list_num[0], self)
        self.test1.move(100, 100)
        self.tb = QLineEdit(self)
        self.tb.move(100, 150)
        self.test2 = QLabel("                                                    ", self)
        self.test2.move(50, 250)
        # self.bt1 = QPushButton('查看正确答案', self)
        # self.bt1.move(20, 350)
        self.bt2 = QPushButton('下一题', self)
        self.bt2.move(300, 350)
        self.show()
        # self.bt1.clicked.connect(self.showDialog)
        self.bt2.clicked.connect(self.showDialog)

    def showDialog(self):
        sender = self.sender()
        if self.i < len(list_num):
            if sender == self.bt2:
                # 显示下一题
                cal = eval(list_num[self.i])
                result = int(self.tb.text())
                if cal == result:
                    self.test2.setText("you are right")
                else:
                    self.test2.setText("you are wrong.the right answer is %s" % cal)
                self.i += 1
                self.test1.setText(list_num[self.i])
                self.tb.setText("")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = calculate()
    sys.exit(app.exec_())
