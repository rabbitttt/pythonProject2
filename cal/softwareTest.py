"""
@author : Administrator
@Date : 2022/3/28  16:03
"""
import random

list_num = []
ope = ["+", "-", "*", "/"]


def program():
    num = 10  # int(input("需要打印题目的数量:"))
    num_range = 100  # int(input("请输入数字范围："))
    while num >= 1:
        first = random.randint(0, num_range)
        second = random.randint(0, num_range)
        third = random.randint(0, num_range)
        sign_1 = random.randint(0, 3)  # 0为加法，1为减法，2为乘法，3为除法
        sign_2 = random.randint(0, 3)  # 0为加法，1为减法，2为乘法，3为除法
        brackets = random.randint(0, 2)  # 0为无括号，1为前俩位，2为后两个
        # 括号位置：前俩个，后两个，无
        if brackets == 0:
            num_str = str(first) + ope[sign_1] + str(second) + ope[sign_2] + str(third)
        elif brackets == 1:
            num_str = '(' + str(first) + ope[sign_1] + str(second) + ')' + ope[sign_2] + str(third)
        else:
            num_str = str(first) + ope[sign_1] + '(' +str(second) + ope[sign_2] + str(third)+ ')'
        # 重复性判断
        if num_str not in list_num:
            list_num.append(num_str)
            # 除数非零判断
        if sign_1 == 3 and ((second == 0 & brackets != 2) or (brackets == 2 & third == 0)):
            continue
        if sign_2 == 3 and third == 0:
            continue
            # 正误判断
        # print(num_str + '=?')
        # cal = eval(num_str)
        # result = int(input("你的计算结果是："))
        # if cal == result:
        #     print("you are right")
        # else:
        #     print("you are wrong.the right answer is %s" % cal)
        num -= 1
    return list_num
