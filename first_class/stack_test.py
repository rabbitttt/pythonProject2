"""
@author : rabbit
@Date : 2022/3/26  9:20
"""


# 给一个点，我们能够根据这个点知道一些内容
class Node(object):
    def __init__(self, val):  # 定位的点的值和一个指向
        self.val = val  # 指向元素的值
        self.next = None  # 指向的指针


class Stack(object):
    def __init__(self):
        self.top = None  # 初始化最开始的位置

    def peek(self):  # 获取栈顶的元素
        if self.top is not None:  # 如果栈顶不为空
            return self.top.val  # 返回栈顶元素的值
        else:
            return None

    def push(self, n):  # 添加到栈中--入栈
        n = Node(n)  # 实例化节点
        n.next = self.top  # 顶端元素传值给一个指针
        self.top = n  # 新入栈的节点作为栈顶元素
        return n.val

    def pop(self):  # 出栈
        if self.top is None:
            return None
        else:
            tmp = self.top.val
            self.top = self.top.next  # 栈顶下移一位
            return tmp

# if __name__=="__main__":
#     s=Stack()
#     s.push(1)
#     s.push(2)
#     s.push(3)
#
#     print(s.pop())
#     print(s.pop())
#     print(s.pop())
